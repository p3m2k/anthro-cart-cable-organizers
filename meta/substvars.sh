# SLUG and RELEASE_VERSION are used in meta/install.txt, which can't
# abide whitespace!

# Should never change
NAME="Anthro Cart Cable Organizers"
SLUG="anthro_cart_cable_organizers"
DESCRIPTION="Simple, 3-D-printable cable organizers designed to attach to the tubular uprights of Anthro-brand carts and desks."
AUTHOR_NAME="Paul Mullen"
AUTHOR_CONTACT="pm@nellump.net"
COPYRIGHT_OWNER="Paul Mullen"
COPYRIGHT_CONTACT="pm@nellump.net"
LICENSE_SPDX_ID="MIT"

# May need updating
COPYRIGHT_YEARS="2017-2023"
RELEASE_VERSION="1.0.0"
RELEASE_DATE="2023-01-10"
