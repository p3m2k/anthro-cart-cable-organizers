############################
Anthro Cart Cable Organizers
############################


Introduction
============

AnthroCart_ brand desks and equipment carts are ideal for piling up all
of your computer and electronics gear.  Eventually, you'll have a mess
of wires running all over the furniture.

.. _AnthroCart: https://www.ergotron.com/en-us/search-results?s=AnthroCart
.. _AnthroCart-WayBack: https://web.archive.org/web/20170505103051/http://www.anthro.com/solutions/Sit-Stand-Desks/anthrocart

This repo contains design files for a simple cable organizer that clamps
to the tubular uprights of an Anthro cart.  The organizer assembly
consists of two, semi-circular halves.  The rear half contains a pair of
`heat-set threaded inserts`_ into which screws can be fastened from the
front half.

.. _heat-set threaded inserts: https://www.mcmaster.com/#94180a353

.. figure:: doc/images/cable_organizer-assembly.jpeg
   :figwidth: 50%
   :width: 100%
   :alt: Isometric CAD view of cable oragnizer assembly.

   Isometric CAD view of cable organizer assembly.

The parts are designed with Solvespace_, a free (GPLv3) parametric 3D
CAD tool.  STL files generated from the Solvespace source can be found
in the ``/build`` directory.

.. _Solvespace: http://solvespace.com/



Future Plans
============

1. Beef up the area of the rear half where the screw hole tabs
   transition to the main "strap," to reduce the chance of stress
   fractures.

2. Consider a re-design that eliminates the use of threaded inserts and
   screws, and instead utilizes a snap-fit latch to secure the two
   halves to each other.  The threaded inserts and screws are too much
   trouble.



Copying
=======

Everything here is copyright © 2017-2023 Paul Mullen
(with obvious exceptions for any `fair use`_ of third-party content).

Everything here is offered under the terms of the MIT_ license.

    Basically, you can do whatever you want as long as you include the
    original copyright and license notice in any copy of the
    software/source.

.. _fair use: http://fairuse.stanford.edu/overview/fair-use/what-is-fair-use/
.. _MIT: https://tldrlegal.com/license/mit-license



Contact
=======

Feedback is (almost) always appreciated.  Send e-mail to
pm@nellump.net.
